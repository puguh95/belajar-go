package v1

import (
	"github.com/gin-gonic/gin"
)

//RoutesV1 is a function of groups of routes V1
func RoutesV1(router *gin.Engine, parentRoute *gin.RouterGroup) *gin.Engine {

	v1 := parentRoute.Group("v1")
	{
		tes := v1.Group("tes")
		{
			tes.GET("/ping", func(c *gin.Context) {
				c.JSON(200, gin.H{
					"message": "pong",
				})
			})
		}

		//registering routes user
		RoutesUser(router, v1)
	}

	return router
}
