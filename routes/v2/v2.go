package v2

import (
	"github.com/gin-gonic/gin"
)

//RoutesV2 is a function of groups of routes V2
func RoutesV2(router *gin.Engine, parentRoute *gin.RouterGroup) *gin.Engine {

	v2 := parentRoute.Group("v2")
	{
		tes := v2.Group("tes")
		{
			tes.GET("/ping", func(c *gin.Context) {
				c.JSON(200, gin.H{
					"message": "pong V2",
				})
			})
		}
	}

	return router
}
