package routes

import (
	"./v1"
	"./v2"
	"github.com/gin-gonic/gin"
)

//RoutesMain is a main routes, base group route is api
func RoutesMain() *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	api := router.Group("api")
	{
		v1.RoutesV1(router, api)
		v2.RoutesV2(router, api)
	}

	return router
}
