package controllers

import (
	"net/http"

	"../databases"
	"../models"
	"github.com/gin-gonic/gin"

	uuid "github.com/satori/go.uuid"
)

//UserController is a struct
type UserController struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

var userModel = new(models.User)

//GetUser is function to get user
func (u UserController) GetUser(c *gin.Context) {

}

//GetUserByUserName is a function to get user by username
func (u UserController) GetUserByUserName(c *gin.Context) {
	db := databases.GetDB()
	var user models.User

	var data = db.Where("username = ?", c.Query("username")).Find(&user)

	if data.RowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "User Not Found", "data": &data.Value})
	} else {
		c.JSON(http.StatusFound, gin.H{"status": http.StatusFound, "message": "User Found", "data": &data.Value})
	}
}

//CreateUser is function to create user
func (u UserController) CreateUser(c *gin.Context) {
	db := databases.GetDB()
	id := uuid.Must(uuid.NewV4())

	var user UserController

	c.BindJSON(&user)

	userModel := models.User{
		UserID:   id.String(),
		Username: user.Username,
		Password: user.Password,
		Email:    user.Email,
	}

	db.Create(&userModel)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "User created successfully!", "resourceId": userModel.UserID})
}

//UpdateUser is a function to update user , reflect.Indirect(reflect.ValueOf(&user)).FieldByName("Username").String()
func (u UserController) UpdateUser(c *gin.Context) {
	db := databases.GetDB()
	var newUser UserController
	var dataExist models.User
	c.BindJSON(&newUser)

	db.Where("username = ?", newUser.Username).Find(&dataExist)
	if &dataExist == nil {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "User Not Found", "data": dataExist})
	} else {
		db.Model(dataExist).Where("user_id = ?", dataExist.UserID).Updates(models.User{Password: newUser.Password, Email: newUser.Email})
	}

	c.JSON(http.StatusFound, gin.H{"status": http.StatusFound, "message": "User Found", "data": dataExist, "user": newUser})

}
