package models

// User is a model for user
type User struct {
	UserID   string `json:"UserID"`
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

// TransformedUser is a model for response api
type TransformedUser struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}
